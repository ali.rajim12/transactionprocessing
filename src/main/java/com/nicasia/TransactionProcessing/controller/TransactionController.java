package com.nicasia.TransactionProcessing.controller;

import com.nicasia.TransactionProcessing.dto.CreateTxnDto;
import com.nicasia.TransactionProcessing.dto.GenericResponse;
import com.nicasia.TransactionProcessing.service.TransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping(value = "/api/v1/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @PostMapping
    public GenericResponse createTransactionDetail(@RequestBody
                                                   CreateTxnDto createTxnDto) {
        return transactionService.createTransctionDetail(createTxnDto);
    }


}
