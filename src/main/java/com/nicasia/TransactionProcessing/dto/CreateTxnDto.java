package com.nicasia.TransactionProcessing.dto;

import lombok.Data;

import java.util.*;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Data
public class CreateTxnDto {
    private String initiator;
    private double amount;
    private String remarks;
    private String uniqueId;
    private Map<String, String> props;
    private List<TxnJournalDto> txnJournalDtoList;
}
