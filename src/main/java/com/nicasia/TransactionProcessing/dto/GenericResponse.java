package com.nicasia.TransactionProcessing.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Data
@Builder
public class GenericResponse {
    private int code;
    private String message;
    private String paymentStatus;
    private String uniqueId;
}
