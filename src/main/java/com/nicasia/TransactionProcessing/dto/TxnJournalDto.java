package com.nicasia.TransactionProcessing.dto;

import lombok.Data;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Data
public class TxnJournalDto {
    private String fromAccount;
    private String toAccount;
    private double amount;
    private String type;
    private String remarks;
}
