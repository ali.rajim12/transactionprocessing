package com.nicasia.TransactionProcessing.model;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public enum Status {
    INACTIVE, ACTIVE, DELETED
}
