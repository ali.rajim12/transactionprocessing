package com.nicasia.TransactionProcessing.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Data
@Entity
public class TransactionDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModified;

    @Version
    private Long version = 1L;

    private double amount;
    @Column(length = 45)
    private String remarks;
    @Column(unique = true)
    private String uniqueId;

    private Date txnInitiatedDate;
    private Date txnCompletedDate;

    private PaymentStatus paymentStatus;

    private Status status;

    private String initiatorId;
}
