package com.nicasia.TransactionProcessing.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Data
@Entity
public class TransactionProps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModified;

    @Version
    private Long version = 1L;

    private Status status;

    private String keyData;

    private String valueData;

    @ManyToOne
    @JoinColumn(name = "transaction_detail_id")
    private TransactionDetail transactionDetail;

}
