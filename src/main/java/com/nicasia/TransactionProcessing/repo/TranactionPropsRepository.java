package com.nicasia.TransactionProcessing.repo;

import com.nicasia.TransactionProcessing.model.TransactionJournal;
import com.nicasia.TransactionProcessing.model.TransactionProps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Repository
public interface TranactionPropsRepository
        extends JpaRepository<TransactionProps, Long> {
}
