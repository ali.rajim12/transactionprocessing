package com.nicasia.TransactionProcessing.repo;

import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.model.TransactionJournal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Repository
public interface TransactionJournalRepository
        extends JpaRepository<TransactionJournal, Long> {
}
