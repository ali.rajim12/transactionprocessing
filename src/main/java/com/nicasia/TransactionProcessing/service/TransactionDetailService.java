package com.nicasia.TransactionProcessing.service;

import com.nicasia.TransactionProcessing.dto.CreateTxnDto;
import com.nicasia.TransactionProcessing.model.TransactionDetail;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public interface TransactionDetailService {
    TransactionDetail createTxnDetail(CreateTxnDto createTxnDto);
}
