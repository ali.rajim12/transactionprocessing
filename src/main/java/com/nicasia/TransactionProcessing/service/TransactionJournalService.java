package com.nicasia.TransactionProcessing.service;

import com.nicasia.TransactionProcessing.dto.TxnJournalDto;
import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.model.TransactionJournal;

import java.util.List;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public interface TransactionJournalService {
    void createTransactionJournal(TransactionDetail transactionDetail,
                                                      List<TxnJournalDto> txnJournalDtoList);
}
