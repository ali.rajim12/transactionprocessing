package com.nicasia.TransactionProcessing.service;

import com.nicasia.TransactionProcessing.model.TransactionDetail;

import java.util.Map;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public interface TransactionPropService {
    void createProps(Map<String, String> props, TransactionDetail transactionDetail);
}
