package com.nicasia.TransactionProcessing.service;

import com.nicasia.TransactionProcessing.dto.CreateTxnDto;
import com.nicasia.TransactionProcessing.dto.GenericResponse;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public interface TransactionService {
    GenericResponse createTransctionDetail(CreateTxnDto createTxnDto);
}
