package com.nicasia.TransactionProcessing.service.impl;

import com.nicasia.TransactionProcessing.dto.CreateTxnDto;
import com.nicasia.TransactionProcessing.model.PaymentStatus;
import com.nicasia.TransactionProcessing.model.Status;
import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.repo.TransactionDetailRepository;
import com.nicasia.TransactionProcessing.service.TransactionDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class TransactionDetailServiceImpl
        implements TransactionDetailService {
    private final TransactionDetailRepository transactionDetailRepository;

    public TransactionDetailServiceImpl(TransactionDetailRepository transactionDetailRepository) {
        this.transactionDetailRepository = transactionDetailRepository;
    }

    @Override
    public TransactionDetail createTxnDetail(CreateTxnDto createTxnDto) {
        log.info("Creating txn detail .. ");
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setAmount(createTxnDto.getAmount());
        transactionDetail.setCreatedDate(new Date());
        transactionDetail.setInitiatorId(createTxnDto.getInitiator());
        transactionDetail.setLastModified(new Date());
        transactionDetail.setPaymentStatus(PaymentStatus.PENDING);
        transactionDetail.setRemarks(createTxnDto.getRemarks());
        transactionDetail.setStatus(Status.ACTIVE);
        transactionDetail.setUniqueId(createTxnDto.getUniqueId());
//        transactionDetail.setTxnInitiatedDate(new Date());
        return transactionDetailRepository.save(transactionDetail);
    }
}
