package com.nicasia.TransactionProcessing.service.impl;

import com.nicasia.TransactionProcessing.dto.TxnJournalDto;
import com.nicasia.TransactionProcessing.model.Status;
import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.model.TransactionJournal;
import com.nicasia.TransactionProcessing.model.TxnType;
import com.nicasia.TransactionProcessing.repo.TransactionJournalRepository;
import com.nicasia.TransactionProcessing.service.TransactionJournalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class TransactionJournalServiceImpl
        implements TransactionJournalService {
    private final TransactionJournalRepository transactionJournalRepository;

    public TransactionJournalServiceImpl(TransactionJournalRepository transactionJournalRepository) {
        this.transactionJournalRepository = transactionJournalRepository;
    }

    @Override
    public void createTransactionJournal(TransactionDetail transactionDetail,
                                         List<TxnJournalDto> txnJournalDtoList) {
        // WIth variable defined
//        List<TransactionJournal> transactionJournalList = txnJournalDtoList
//                .stream()
//                .map(this::createTxnJournal)
//                .collect(Collectors.toList());
        // old style for loop
        List<TransactionJournal> transactionJournalList = new ArrayList<>();
        for(TxnJournalDto txnJournalDto:  txnJournalDtoList) {
            transactionJournalList.add(createTxnJournal(txnJournalDto, transactionDetail));
        }

        // With stream without varialbe
        transactionJournalRepository.saveAll(txnJournalDtoList
                .stream()
                .map(e-> createTxnJournal(e, transactionDetail))
                .collect(Collectors.toList()));
    }

    private TransactionJournal createTxnJournal(TxnJournalDto txnJournalDto,
                                                TransactionDetail transactionDetail) {
        TransactionJournal transactionJournal = new TransactionJournal();
        transactionJournal.setAmount(txnJournalDto.getAmount());
        transactionJournal.setCreatedDate(new Date());
        transactionJournal.setFromAccount(txnJournalDto.getFromAccount());
        transactionJournal.setLastModified(new Date());
        transactionJournal.setRemarks(txnJournalDto.getRemarks());
        transactionJournal.setStatus(Status.ACTIVE);
        transactionJournal.setToAccount(txnJournalDto.getToAccount());
        transactionJournal.setTxnType(TxnType.valueOf(txnJournalDto.getType()));
        transactionJournal.setTransactionDetail(transactionDetail);
        return transactionJournal;
    }
}
