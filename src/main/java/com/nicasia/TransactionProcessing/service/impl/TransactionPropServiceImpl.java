package com.nicasia.TransactionProcessing.service.impl;

import com.nicasia.TransactionProcessing.model.Status;
import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.model.TransactionProps;
import com.nicasia.TransactionProcessing.repo.TranactionPropsRepository;
import com.nicasia.TransactionProcessing.service.TransactionPropService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class TransactionPropServiceImpl
        implements TransactionPropService {
    private final TranactionPropsRepository tranactionPropsRepository;

    public TransactionPropServiceImpl(TranactionPropsRepository tranactionPropsRepository) {
        this.tranactionPropsRepository = tranactionPropsRepository;
    }

    @Override
    public void createProps(Map<String, String> props,
                            TransactionDetail transactionDetail) {
        log.info("Creating props ...");
        List<TransactionProps> transactionPropsList = props.entrySet()
                .stream()
                .map(e-> convertToTxnProp(e, transactionDetail))
                .collect(Collectors.toList());
        tranactionPropsRepository.saveAll(transactionPropsList);
    }

    private TransactionProps convertToTxnProp(Map.Entry<String, String> e,
                                              TransactionDetail transactionDetail) {
        TransactionProps transactionProps = new TransactionProps();
        transactionProps.setCreatedDate(new Date());
        transactionProps.setLastModified(new Date());
        transactionProps.setKeyData(e.getKey());
        transactionProps.setValueData(e.getValue());
        transactionProps.setStatus(Status.ACTIVE);
        transactionProps.setTransactionDetail(transactionDetail);
        return transactionProps;
    }
}
