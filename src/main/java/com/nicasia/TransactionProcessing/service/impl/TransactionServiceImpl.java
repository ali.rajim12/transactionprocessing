package com.nicasia.TransactionProcessing.service.impl;

import com.nicasia.TransactionProcessing.dto.CreateTxnDto;
import com.nicasia.TransactionProcessing.dto.GenericResponse;
import com.nicasia.TransactionProcessing.model.TransactionDetail;
import com.nicasia.TransactionProcessing.service.TransactionDetailService;
import com.nicasia.TransactionProcessing.service.TransactionJournalService;
import com.nicasia.TransactionProcessing.service.TransactionPropService;
import com.nicasia.TransactionProcessing.service.TransactionService;
import com.nicasia.TransactionProcessing.shared.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    private final TransactionDetailService transactionDetailService;

    private final TransactionJournalService transactionJournalService;

    private final TransactionPropService transactionPropService;

    public TransactionServiceImpl(TransactionDetailService transactionDetailService,
                                  TransactionJournalService transactionJournalService,
                                  TransactionPropService transactionPropService) {
        this.transactionDetailService = transactionDetailService;
        this.transactionJournalService = transactionJournalService;
        this.transactionPropService = transactionPropService;
    }

    @Override
    @Transactional
    public GenericResponse createTransctionDetail(CreateTxnDto createTxnDto) {
        log.info("Creating transaction detail with :: {}", createTxnDto.toString());
        // TODO validation
        if(createTxnDto.getTxnJournalDtoList().size() == 0) {
            // TODO make message dynamic
            return GenericResponse.builder()
                    .code(ResponseCode.TXN_JOURNAL_REQUIRED)
                    .message("Txn Journal not found.")
                    .build();
        }
        TransactionDetail transactionDetail = transactionDetailService.createTxnDetail(createTxnDto);

        transactionJournalService.createTransactionJournal(transactionDetail,
                createTxnDto.getTxnJournalDtoList()
                );

        if(createTxnDto.getProps().size() > 0) {
            transactionPropService.createProps(createTxnDto.getProps(), transactionDetail);
        }

        // TODO call third party api
        // set initated date
        return null;
    }
}
