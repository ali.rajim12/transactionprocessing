package com.nicasia.TransactionProcessing.shared;

/**
 * @author rajim 2021-09-28.
 * @project IntelliJ IDEA
 */
public class ResponseCode {
    public static final int TXN_JOURNAL_REQUIRED = 400;
}
